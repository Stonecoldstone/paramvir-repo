# Paramvir Repo

# Usage:
`python main.py -p Son Dad Mom -p Son1 Dad1 Mom1 Daughter1 -p Son2 Dad2 Mom2`

# Questions:
## 1. Describe your algorithm for selecting Secret Santas
To find secret santas within a list of family members, one creates copy of family members, selects each person sequentially,
choses random member from list and assigns it as a secret santa to the person, with a condition that the person is not
equal to the secret santa and it is in copy of family members. After secret santa is assigned, one removes it from family members copy.
It retries this process until all persons have secret santas.
To sort out immediate family members, one just compares if santa in immediate family members and retries random choice if it is.
## 2. What are the runtime characteristics of your final solution?
Finding secret santas for list of family members of length 9 takes 0.09 seconds average for 100 runs.
Finding secret santas for list of family members of length 1000 takes 0.34 seconds average for 100 runs.
## 3. What are the memory characteristics of your final solution?
It is 29 mb for list of family members of length 1000.
And 25 mb for list of family members of length of 9.
## 4. How can/did you structure your application such that adding additional constraints would require minimal code changes?
By making separate function for calculation process, so if finding secret santa fails and runs into infinite loop, it can be restarted.
This issue was even without additional restrictions and appeared frequently after adding them. For me, it was adding database to
keep track of previous years results and adding that results as restriction to solve part 2. To solve part 3, input had to be refactored
to list groups of immediate family members, loop changed to work with new datatype and additional condition was added: is secret santa immediate family member of the person.
## 5. How can/did you structure your application such that a transition from an in-memory datastore to a persistent datastore would require minimal code changes?
Would probably structured the same, but substituted in-memory datatypes with persistent datastore. Also, there would have been more calls to database, in case of sqlite3.
## 6. If you had to choose a persistent datastore to use for this application, what would you use and why?
I'd use sqlite3 as it requires minimal settings both in code and in installation, configuring database. If program scales to multithreaded, I'd use PostgreSQL, because it has good multithread support.
## 7. Let’s imagine the entire population of the United States is your extended family, how do you scale your application to generate Secret Santas?
I probably would have need to write program to split all population, so there would be collection of immediate family members.
Also, I'd switch to persistent datastore like sqlite or PostgreSQL, so memory usage of the program will be less.
The whole algorithm might be changed, as repeating it to get results (because of infinite loop issue) might be too long for such number of family members.
