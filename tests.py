import unittest
from collections import defaultdict
from main import secret_santa


class SecretSantaTest(unittest.TestCase):

    def test_same_name_passes(self):
        persons = [['Mom', 'Dad', 'Dad', 'Son'], ['Mom1', 'Son1', 'Daughter1', 'Friend1'], ['Mom2', 'Dad2', 'Son2']]
        res = secret_santa(persons)
        persons = [item for items in persons for item in items]
        for person in persons:
            self.assertIn(person, res.keys())
            self.assertIn(person, res.values())

    def test_all_santas_all_gifted(self):
        persons = [['Mom,', 'Dad', 'Son', 'Daughter', 'Friend'], ['Mom1', 'Son1', 'Dad1'], ['Mom2', 'Dad2', 'Son2']]
        res = secret_santa(persons)
        persons = [item for items in persons for item in items]
        for person in persons:
            self.assertIn(person, res.keys())
            self.assertIn(person, res.values())

    def test_santas_different(self):
        persons = [['Mom', 'Dad', 'Son', 'Daughter', 'Friend'], ['Mom1', 'Son1', 'Dad1'], ['Mom2', 'Son2', 'Dad2']]
        all_results = defaultdict(list)
        for i in range(3):
            res = secret_santa(persons)
            for person, santa in res.items():
                all_results[person].append(santa)
        for person, santas in all_results.items():
            self.assertEqual(len(set(santas)), 3)


    def test_family_members(self):
        persons = [['Mom', 'Dad', 'Son', 'Daughter', 'Friend'], ['Mom'], ['Dad']]
        res = secret_santa(persons)
        self.assertIsNone(res)


if __name__ == '__main__':
    unittest.main()