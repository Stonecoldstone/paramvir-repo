import argparse
from collections import defaultdict
import random
import sys
import sqlite3


def make_choices(c, persons, all_persons, last_two_years):
    family_i = 0
    person_i = 0
    all_persons = list(all_persons)
    last_choices = defaultdict(list)
    for person, santa, _ in last_two_years:
        last_choices[person].append(santa)
    secret_santas = {}
    while True:
        try:
            family = persons[family_i]
        except IndexError:
            break
        try:
            person = family[person_i]
        except IndexError:
            family_i += 1
            person_i = 0
            continue
        last_person_choices = last_choices[person]
        restart = True
        for member in all_persons:
            if member not in family and member not in last_person_choices:
                restart = False
                break
        if restart:
            print('None  returned')
            return None
        santa = random.choice(all_persons)
        if santa in family:
            continue
        if santa != person and santa not in last_person_choices:
            secret_santas[person] = santa
            all_persons.remove(santa)
            person_i += 1
    return secret_santas


def secret_santa(persons):
    if len(persons) == 1:
        print('There must be multiple families')
        return
    db = sqlite3.connect('main.db')
    c = db.cursor()
    c.execute('create table if not exists secret_santa (person text, santa text, year integer)')
    persons = [list(set(item)) for item in persons]
    all_persons = [item for items in persons for item in items]
    limit_num = len(all_persons) * 2
    c.execute('select person, santa, year from secret_santa order by -year limit ?', (limit_num,))
    last_two_years = c.fetchall()
    secret_santas = None
    num_tries = 100
    while not secret_santas:
        secret_santas = make_choices(c, persons, all_persons, last_two_years)
        num_tries -= 1
        if num_tries == 0:
            print('Failed to get secret santas for {}'.format(persons))
            return None

    for person, santa in secret_santas.items():
        if last_two_years:
            last_year = last_two_years[0][-1] + 1
        else:
            last_year = 1
        c.execute('insert into secret_santa values (?, ?, ?)', (person, santa, last_year))
        print('{}: {}'.format(person, santa))
    db.commit()
    return secret_santas


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Chooses secret santas for each person')
    parser.add_argument('-p', '--persons', nargs='+', action='append', help='Immediate family members')
    args = parser.parse_args(sys.argv[1:])
    secret_santa(args.persons)
